import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import random
import matplotlib.pyplot as plt
import dbModel as db
import datetime
import NathanSupportFunctions as NSF

NSFGraphFuncs = NSF.GraphFuncs()
NSFDataManu = NSF.DataManipulation()

fig = NSFGraphFuncs.basicGraphSetUp('Monthly Traffic for 8019 - Year: 2020','Site: 8019','Traffic Total')

bar_width = 0.25
opacity = 0.8

monthsOfYear = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November','December']
Start = '2020-01-01 00:00:00'
End = '2020-02-01 00:00:00'

for month in range(0,8):
    trafficRecords = db.getSomeTrafficCountPerMonth(Start,End)

    objects = []
    performance = []

    for record in trafficRecords:
        performance.append(record[1])

    plt.bar(np.arange(len(performance)) + (month * bar_width), performance, bar_width, alpha=opacity, label=monthsOfYear[month])
    Start = NSFDataManu.addAMonth(Start)
    End = NSFDataManu.addAMonth(End)

y_pos = np.arange(len(performance))
plt.xticks(y_pos, objects , rotation=50)
plt.legend()

fig.tight_layout()
NSFGraphFuncs.testSave(fig)
NSFGraphFuncs.finalSave(fig,'MonthlyTrafficFor8019-2020')
NSFGraphFuncs.showPlot(plt,20)