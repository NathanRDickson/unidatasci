import csv
import pandas as pd

def read_traffic_file(fileName,day):
    with open(fileName) as file:
        traffic_data = list(csv.reader(file))
        site = traffic_data[0][1]
        date = traffic_data[2][4]
        traffic_data_for_day = convert_arrays(traffic_data,day)
    return site,date,traffic_data_for_day

def read_air_file(fileName):
    with open(fileName) as file:
        air_data = list(csv.reader(file))
    return air_data[3:]

def convert_arrays(traffic_data,day):
    traffic_data_for_day = []
    for row in traffic_data[6:1446]:
        traffic_data_for_day.append(row[day])    
    return traffic_data_for_day