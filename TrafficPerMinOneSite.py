import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import datetime
import dbModel as db
import time
import random
import NathanSupportFunctions as NSF

#Graph Set Up
Site = 8019
GraphDate = '2020-02-10 00:00:00'
Start = '2020-02-10 00:00:00'
End = '2020-02-10 23:59:00'

traffic = db.getTrafficData(Site,Start,End)
trafficCount = NSF.moving_average(traffic,100)

fig = plt.figure(figsize=(15,6))

#Plot Lables
plt.title("Smoothed Traffic Count Per Min", fontsize=15) 
plt.xlabel('Time and Date')
plt.ylabel('Traffic (Cars per min)')

#Aesthetic
plt.subplots_adjust(bottom=0.35)

#DateAesthetic
ax=plt.gca()
xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
ax.xaxis.set_major_formatter(xfmt)

#Plot
for day in range(0, 7):
    dayOfWeek = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday')
    traffic = db.getTrafficData(Site,Start,End)
    trafficCount = NSF.moving_average(traffic,100)
    datetimeAX = NSF.time_convertion(GraphDate,len(trafficCount))
    if day < 5:
        plt.plot(datetimeAX,trafficCount,linewidth=0)
    else: 
        plt.plot(datetimeAX,trafficCount, label = "FOXHALL ROAD - " + str(Site) + " - Day: " + dayOfWeek[day],linewidth=1)
    Start = NSF.addADay(Start)
    End = NSF.addADay(End)

plt.legend(title='Day Of Week', bbox_to_anchor=(1.05, 1), loc='upper left')

fig.tight_layout()
NSF.testSave(fig)
NSF.finalSave(fig,'8019_Weekend_20200210')
NSF.showPlot(plt,200)