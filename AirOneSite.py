import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import datetime
import dbModel as db
import time
import random
import NathanSupportFunctions as NSF

NSFGraphFuncs = NSF.GraphFuncs()
NSFDataManu = NSF.DataManipulation()

fig = NSFGraphFuncs.basicGraphSetUp('Air Data 10/02/2020','Air Quality Across Ipswich','NO2')
#Graph Set Up
Site = 1
GraphDate = '2020-02-10 00:00:00'
Start = '2020-02-10 00:00:00'
End = '2020-02-10 23:59:00'

air = db.getAirData(Site,Start,End)
air = NSFDataManu.moving_average(air,20)
print(len(air))

#Aesthetic
plt.subplots_adjust(bottom=0.35)
plt.xticks( rotation=50 )

ax=plt.gca()
xfmt = md.DateFormatter('%H:%M:%S'))
ax.xaxis.set_major_formatter(xfmt)

i= 0
x= []

print (len(air))
for airQULevel in range(len(air)):
    i +=1
    offset = i * 15
    starttime=datetime.datetime.strptime(GraphDate,'%Y-%m-%d %H:%M:%S')
    x.append(starttime + datetime.timedelta(minutes=offset))

print air
plt.plot(x,air, label = "N02",linewidth=1)


plt.legend(title='Particle Type', bbox_to_anchor=(1.05, 1), loc='upper left')

fig.tight_layout()
NSFGraphFuncs.testSave(fig)
NSFGraphFuncs.finalSave(fig,'8019_AirQuality_20200210')
NSFGraphFuncs.showPlot(plt,200)