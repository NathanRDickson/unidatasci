import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import datetime as dt
import time
import random
import FileReader as fr
from dateutil.relativedelta import relativedelta

class GraphFuncs:   
    def basicGraphSetUp(self,graphTitle,Xlabel,YLabel):
        fig = plt.figure(figsize=(15,6))

        plt.title(graphTitle, fontsize=15) 
        plt.xlabel(Xlabel)
        plt.ylabel(YLabel)

        plt.subplots_adjust(bottom=0.35)
        return fig

    def showPlot(self,plot,showFor):
        plot.show(block=False)
        plot.pause(showFor)
        plot.close()
        return "Done"

    def testSave(self,graph):
        randSave = 'outputs/tests/test'+str(random.randint(1,100001))+'.png'
        graph.savefig(randSave)

    def finalSave(self,graph,fileName):
        randSave = 'outputs/finals/'+fileName+'.png'
        graph.savefig(randSave)


class DataManipulation:
    def time_convertion(self,date,total):
        duration=86400
        starttime=time.mktime(time.strptime(date,'%Y-%m-%d %H:%M:%S'))
        timestamps=np.linspace(starttime,starttime+duration,total)
        dates=[dt.datetime.fromtimestamp(ts) for ts in timestamps]
        return md.date2num(dates)
    
        def time_convertion(self,date,total):
            duration=86400
            starttime=time.mktime(time.strptime(date,'%Y-%m-%d %H:%M:%S'))
            timestamps=np.linspace(starttime,starttime+duration,total)
            dates=[dt.datetime.fromtimestamp(ts) for ts in timestamps]
            return md.date2num(dates)

    def getTrafficCount(self,smoothing,day):
        file='Data/Traffic_8019_200210.csv'
        traffic = np.array(map(int, fr.read_traffic_file(file,day)))
        return moving_average(traffic,smoothing)

    def moving_average(self,trafficData, smoothing) :
        trafficData = np.cumsum(trafficData, dtype=float)
        trafficData[smoothing:] = trafficData[smoothing:] - trafficData[:-smoothing]
        return trafficData[smoothing - 1:] / smoothing

    def addADay(self,dateTimeToChange):
        dateTimeToChange = dt.datetime.strptime(dateTimeToChange, '%Y-%m-%d %H:%M:%S')
        dateTimeToChange = dateTimeToChange + dt.timedelta(minutes = 1440)
        dateTimeToChange = dateTimeToChange.strftime("%Y-%m-%d %H:%M:%S")
        return dateTimeToChange

    def addAMonth(self,dateTimeToChange):
        dateTimeToChange = dt.datetime.strptime(dateTimeToChange, '%Y-%m-%d %H:%M:%S')
        dateTimeToChange = dateTimeToChange + relativedelta(months=1)
        dateTimeToChange = dateTimeToChange.strftime("%Y-%m-%d %H:%M:%S")
        return dateTimeToChange