import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import dbModel as db
import NathanSupportFunctions as NSF

NSFGraphFuncs = NSF.GraphFuncs()
NSFDataManu = NSF.DataManipulation()

fig = NSFGraphFuncs.basicGraphSetUp('Traffic at Sites - Feb','Site','Total Traffic')

Start = '2020-02-01 00:00:00'
End = '2020-03-01 00:00:00'
trafficRecords  = db.getTrafficCountPerMonth(Start,End)

objects = []
performance = []

for record in trafficRecords:
    objects.append(record[0])
    performance.append(record[1])

y_pos = np.arange(len(objects))
plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects, rotation=50)

NSFGraphFuncs.testSave(fig)
NSFGraphFuncs.finalSave(fig,'TrafficAtSiteFeb')
NSFGraphFuncs.showPlot(plt,20)