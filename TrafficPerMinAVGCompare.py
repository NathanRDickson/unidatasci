import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import datetime
import dbModel as db
import time
import random
import NathanSupportFunctions as NSF

NSFGraphFuncs = NSF.GraphFuncs()
NSFDataManu = NSF.DataManipulation()

fig = NSFGraphFuncs.basicGraphSetUp("Smoothed Traffic Count Per Min",'Times','Traffic (Cars per min)')

#Graph Set Up
Site =8019
GraphDate = '2019-02-10 00:00:00'
Start = '2020-02-15 00:00:00'
End = '2020-02-15 23:59:00'

traffic = db.getTrafficData(Site,Start,End)
trafficCount = NSFDataManu.moving_average(traffic,100)

#Aesthetic
plt.subplots_adjust(bottom=0.35)
plt.xticks( rotation=50 )


#DateAesthetic
ax=plt.gca()
xfmt = md.DateFormatter('%H:%M:%S')
ax.xaxis.set_major_formatter(xfmt)

#Plot
Sites = [9177,9151,8033,8032,8030,8029,8028,8027,8025,8024,8023,8021,8020,8019,8018,8016,8015,8014,8013,8012,8011,8010,8005,8004]
Sites = [8023,8024,8021]
Sites = [8023,8018]

for Site in Sites:
    SiteTrafficData = []
    SiteTrafficDataAVG = []

    Start = '2020-02-10 00:00:00'
    End = '2020-02-10 23:59:00'
    dayRange = 5
    
    for day in range(0, dayRange):
        dayOfWeek = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday')
        traffic = db.getTrafficData(Site,Start,End)
        trafficCount = NSFDataManu.moving_average(traffic,100)
        datetimeAX = NSFDataManu.time_convertion(GraphDate,len(trafficCount))
        SiteTrafficData.append(trafficCount)
        Start = NSFDataManu.addADay(Start)
        End = NSFDataManu.addADay(End)
    
    SiteTrafficDataAVG = SiteTrafficData[0]
    for day in range(0, dayRange):
        SiteTrafficDataAVG =  SiteTrafficDataAVG + SiteTrafficData[day]
    
    SiteTrafficDataAVG =  SiteTrafficDataAVG / dayRange 
    plt.plot(datetimeAX,SiteTrafficDataAVG, label = str(Site),linewidth=1)

plt.legend(title='Radar Site:', bbox_to_anchor=(1.05, 1), loc='upper left')

fig.tight_layout()
NSFGraphFuncs.testSave(fig)
NSFGraphFuncs.finalSave(fig,'8023,8018_Correlation_10022020')
NSFGraphFuncs.showPlot(plt,200)