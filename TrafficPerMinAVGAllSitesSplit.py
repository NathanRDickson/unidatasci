import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import datetime
import dbModel as db
import time
import random
import NathanSupportFunctions as NSF

NSFGraphFuncs = NSF.GraphFuncs()
NSFDataManu = NSF.DataManipulation()

fig = NSFGraphFuncs.basicGraphSetUp("Smoothed Traffic Count Per Min",'Days','Traffic (Cars per min)')

#Graph Set Up
Site =8019
GraphDate = '2019-02-10 00:00:00'
Start = '2020-02-15 00:00:00'
End = '2020-02-15 23:59:00'

traffic = db.getTrafficData(Site,Start,End)
trafficCount = NSFDataManu.moving_average(traffic,100)

#DateAesthetic
ax=plt.gca()
xfmt = md.DateFormatter('%H:%M:%S')
xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
xfmt = md.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(xfmt)

#Plot
Sites = [9177,9151,8033,8032,8030,8029,8028,8027,8025,8024,8023,8021,8020,8019,8018,8016,8015,8014,8013,8012,8011,8010,8005,8004]
Sites = [8019]

for Site in Sites:
    SiteTrafficData = []
    SiteTrafficDataAVG = []

    Start = '2020-02-10 00:00:00'
    End = '2020-02-16 23:59:00'
    dayRange = 1
    
    for day in range(0, dayRange):
        dayOfWeek = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday')
        traffic = db.getTrafficData(Site,Start,End)
        trafficCount = NSFDataManu.moving_average(traffic,100)
        datetimeAX = NSFDataManu.time_convertion(Start,len(trafficCount))
        SiteTrafficData.append(trafficCount)
        Start = NSFDataManu.addADay(Start)
        End = NSFDataManu.addADay(End)

        plt.plot(datetimeAX,trafficCount, label = str(dayOfWeek[day]),linewidth=1)

fig.tight_layout()
NSFGraphFuncs.testSave(fig)
NSFGraphFuncs.finalSave(fig,'8019_WeekSplit_20200210_SmoothedWeek')
NSFGraphFuncs.showPlot(plt,20)