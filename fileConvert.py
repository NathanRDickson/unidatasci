import pandas as pd
import os
import sys
import FileToDataBase

def xlsxtoCSVConverter(file):
    read_file = pd.read_excel (r''+file+'.xlsx')
    read_file.to_csv (r''+file+'.csv', index = None, header=True)

def removeFile(fileToDelete):
        file = fileToDelete[:-5] + '.csv'
        fileOgin = fileToDelete[:-5] + '.xlsx'
        os.remove(file)
        os.remove(fileOgin)

def findFileToConvert(path):
    Xfiles = []
    Cfiles = []
    for r, d, f in os.walk(path):
        for file in f:
            if '.xlsx' in file:
                Xfiles.append(os.path.join(r, file))
            if '.csv' in file:
                Cfiles.append(os.path.join(r, file))

    for f in Xfiles:
        print(f[:-5])
        xlsxtoCSVConverter(f[:-5])
        file = f[:-5] + '.csv'

 
    for f in Cfiles:
        print(f)
        FileToDataBase.addAirDataToDB(file)


