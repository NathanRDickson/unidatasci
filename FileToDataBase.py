import FileReader as fr
import dbModel as db
import datetime
import time

def addDataToDB(file):
    val = [] 
    for day in range(1,8):
        siteID, fileDate, dayData = fr.read_traffic_file(file,day)
        timestamp = datetime.datetime.strptime(fileDate[12:], '%d %B %Y')
        timestamp = timestamp + datetime.timedelta(days = day-1)
        for minData in dayData:
            val.append((None, siteID, minData, timestamp,str(siteID)+str(timestamp)))
            timestamp = timestamp + datetime.timedelta(minutes = 1)
    sql = "INSERT IGNORE INTO `IpswichTrafficData` (`IpswichTrafficDataID`, `SiteNumber`, `TrafficCount`, `TimeStamp`, `uniqueToken`) VALUES (%s, %s, %s, %s, %s)"
    SQLAdd = db.dbInsertMany(sql, val)

def addAirDataToDB(file):
    fileData = fr.read_air_file(file)
    val = []
    for row in fileData:
        timestamp = datetime.datetime.strptime(row[0], '%d/%m/%Y %H:%M')
        val.append((01, row[1],row[2],row[3],row[4],row[5],row[6],row[7], timestamp,str(01)+str(timestamp)))
    sql = "INSERT IGNORE INTO `IpswichAirData` (`IpswichAirDataID`, `SiteNumber`, `NO2`, `O3`, `PM25`, `PM10`, `TEMP`, `RH`, `DP`, `TimeStamp`, `uniqueToken`) VALUES (Null,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    SQLAdd = db.dbInsertMany(sql, val)