import mysql.connector
import numpy as np

def dbConnect():
    mydb = mysql.connector.connect(
        host = "localhost",
        user = "FruityFrank",
        password = "FruityFrank23!",
        database = "NICData"
    )
    return mydb

def dbInsert(sql, val):
    conn = dbConnect()
    mycursor = conn.cursor()
    mycursor.execute(sql, val)
    conn.commit()
    print("1 record inserted, ID:", mycursor.lastrowid)

def dbInsertMany(sql, val):
    conn = dbConnect()
    mycursor = conn.cursor()
    mycursor.executemany(sql, val)
    conn.commit()
    print("1 record inserted, ID:", mycursor.lastrowid)

def getTrafficData(Site,Start,End):
    conn = dbConnect()
    mycursor = conn.cursor()

    sql = "SELECT * FROM `IpswichTrafficData` WHERE `SiteNumber` = %s AND `TimeStamp` BETWEEN %s AND %s ORDER BY `TimeStamp` ASC"
    val = (Site, Start , End)
    
    mycursor.execute(sql, val)
    result = mycursor.fetchall()
    final_result = [list(i)[2] for i in result]

    conn.close()
    return final_result

def getAirData(Site,Start,End):
    conn = dbConnect()
    mycursor = conn.cursor()

    sql = "SELECT * FROM `IpswichAirData` WHERE `SiteNumber` = %s AND `TimeStamp` BETWEEN %s AND %s ORDER BY `TimeStamp` ASC"
    val = (Site, Start , End)
    
    mycursor.execute(sql, val)
    result = mycursor.fetchall()
    final_result = [list(i)[2] for i in result]

    conn.close()
    return final_result

def getTrafficCountPerMonth(Start,End): 
        conn = dbConnect()
        mycursor = conn.cursor()

        sql = "SELECT `SiteNumber`, Sum(`TrafficCount`), COUNT(`TrafficCount`) FROM `IpswichTrafficData` WHERE `SiteNumber` NOT IN (8017,189,9189) AND `TimeStamp` BETWEEN %s AND %s GROUP BY `SiteNumber` ORDER BY SUM(`TrafficCount`) DESC"
        val = (Start , End)
        
        mycursor.execute(sql, val)
        result = mycursor.fetchall()

        conn.close()
        return result

def getSomeTrafficCountPerMonth(Start,End): 
        conn = dbConnect()
        mycursor = conn.cursor()

        sql = "SELECT `SiteNumber`, Sum(`TrafficCount`), COUNT(`TrafficCount`) FROM `IpswichTrafficData` WHERE `SiteNumber` IN (8019) AND `TimeStamp` BETWEEN %s AND %s GROUP BY `SiteNumber` ORDER BY SUM(`TrafficCount`) DESC"
        val = (Start , End)
        
        mycursor.execute(sql, val)
        result = mycursor.fetchall()

        conn.close()
        return result
